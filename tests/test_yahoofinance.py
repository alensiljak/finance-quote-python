'''
Test the Yahoo Finance
'''
from finance_quote_python.yahoo_finance import YahooFinanceDownloader
from pricedb import SecuritySymbol

def test_yahoo():
    dl = YahooFinanceDownloader()
    symbol = SecuritySymbol('ASX', 'CWNHB')

    result = dl.download(symbol, 'AUD')

    assert result is not None

def test_nasdaq():
    dl = YahooFinanceDownloader()
    symbol = SecuritySymbol('NASDAQ', 'KBWY')

    result = dl.download(symbol, 'USD')

    assert result is not None
    assert result.symbol.mnemonic == 'KBWY'

def test_ams_exchange():
    dl = YahooFinanceDownloader()
    symbol = SecuritySymbol('AMS', 'VFEM')

    result = dl.download(symbol, 'EUR')

    assert result is not None
    assert result.symbol.mnemonic == 'VFEM'

def test_xetra():
    dl = YahooFinanceDownloader()
    symbol = SecuritySymbol('XETRA', 'EL4X')

    result = dl.download(symbol, 'EUR')

    assert result is not None
    assert result.symbol.mnemonic == 'EL4X'

def test_nysearca():
    dl = YahooFinanceDownloader()
    symbol = SecuritySymbol('NYSEARCA', 'EWX')

    result = dl.download(symbol, 'USD')

    assert result is not None
    assert result.symbol.mnemonic == 'EWX'

def test_bvme():
    dl = YahooFinanceDownloader()
    symbol = SecuritySymbol('BVME', 'EMCR')

    result = dl.download(symbol, 'EUR')

    assert result is not None
    assert result.symbol.mnemonic == 'EMCR'
